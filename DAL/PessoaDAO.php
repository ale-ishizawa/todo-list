<?php
/**
 * Created by PhpStorm.
 * User: Alessandro
 * Date: 18/10/2018
 * Time: 15:00
 */

namespace DAL;
require_once ("Conexao.php");
use Model\Pessoa;

class PessoaDAO
{
    private $pdo;
    private $debug;

    public function __construct()
    {
        $this->pdo = new Conexao();
        $this->debug = true;
    }

    public function ListPessoas($search)
    {
        try{
            $sql = "SELECT idpessoa, nome FROM pessoa WHERE nome LIKE '%".$search."%' ";
            $dados = $this->pdo->ExecuteQuery($sql);
            foreach ($dados as $row){
                $response[] = array(
                    "value" => $row['idpessoa'],
                    "label" => $row['nome']
                );
            }

            return json_encode($response);

        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

}