<?php
/**
 * Created by PhpStorm.
 * User: Alessandro
 * Date: 15/10/2018
 * Time: 13:55
 */
namespace DAL;

use Model\Tarefa;

require_once "Conexao.php";
require_once("../Util/ClassSerialization.php");
require_once("../Model/Tarefa.php");

class TarefaDAO
{
    private $pdo;
    private $debug;

    public function __construct()
    {
        $this->pdo = new Conexao();
        $this->debug = true;
        $this->serialize = new \ClassSerialization();
    }

    public function Save(Tarefa $tarefa)
    {
        try{
            $sql = "INSERT INTO tarefa(titulo, descricao, idautor, prazofinal, status) 
                    VALUES(:titulo, :descricao, :idautor, :prazofinal, :status)";
            $param = array(
                ":titulo" => $tarefa->getTitulo(),
                ":descricao" => $tarefa->getDescricao(),
                ":idautor" => $tarefa->autor->getId(),
                ":prazofinal" => $tarefa->getPrazofinal(),
                ":status" => $tarefa->getStatus()
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    public function Edit(Tarefa $tarefa)
    {
        try{
            $sql = "UPADTE tarefa
                SET titulo = :titulo, 
                descricao = :descricao,
                idautor = :idautor,
                prazofinal = :prazofinal,
                status = :status
                WHERE idtarefa = :idtarefa";
            $param = array(
                ":titulo" => $tarefa->getTitulo(),
                ":descricao" => $tarefa->getDescricao(),
                ":idautor" => $tarefa->autor->getId(),
                ":prazofinal" => $tarefa->getPrazofinal(),
                ":status" => $tarefa->getStatus(),
                ":idtarefa" => $tarefa->getIdtarefa()
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }

    }

    public function Delete($idTarefa)
    {
        try{
            $sql = "DELETE FROM tarefa WHERE idtarefa = :idtarefa";
            $param = array(
                ":idtarefa" => $idTarefa
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    public function ListToDo()
    {
        try{
            $sql = "SELECT *, p.nome as autornome FROM tarefa
                INNER JOIN pessoa p on tarefa.idautor = p.idpessoa";
            $dados = $this->pdo->ExecuteQuery($sql);
            $listaTarefas = [];

            foreach($dados as $ta){
                $tarefa = new Tarefa();
                $tarefa->setStatus($ta['status']);
                $tarefa->setPrazofinal($ta['prazofinal']);
                $tarefa->setDescricao($ta['descricao']);
                $tarefa->setTitulo($ta['titulo']);
                $tarefa->setDatacadastro($ta['datacadastro']);
                $tarefa->setAutornome($ta['autornome']);
                $tarefa->setIdtarefa($ta['idtarefa']);
                $listaTarefas[] = $tarefa;
            }

            return $this->serialize->serialize($listaTarefas);
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }
}