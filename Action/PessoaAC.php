<?php

$req = filter_input(INPUT_GET, "req");

if($req){
    require_once "../Controller/PessoaController.php";
    $pessoaController = new PessoaController();

    if($req == "4"){
        if(isset($_POST['search'])) {
            $search = $_POST['search'];
            echo $pessoaController->ListPessoas($search);
        }
    }
}
