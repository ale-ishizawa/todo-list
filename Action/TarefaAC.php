<?php
$req = filter_input(INPUT_GET, "req");

if($req){
    require_once ("../Controller/TarefaController.php");
    $tarefaController = new TarefaController();

    if($req == "1"){
        //Save
        require_once ("../Model/Tarefa.php");
        $tarefa = new \Model\Tarefa();

        $tarefa->setTitulo(filter_input(INPUT_POST, "txtTitulo"));
        $tarefa->setDescricao(filter_input(INPUT_POST, "txtDescricao"));
        $tarefa->autor->setId(filter_input(INPUT_POST, "txtIdAutor"));
        $tarefa->setPrazofinal(filter_input(INPUT_POST, "txtPrazoFinal"));
        $tarefa->setStatus(filter_input(INPUT_POST, "selStatus"));

        echo $tarefaController->Save($tarefa);
    }
    if($req == "2"){
        //Edit
        require_once ("../Model/Tarefa.php");
        $tarefa = new \Model\Tarefa();

        $tarefa->setIdtarefa(filter_input(INPUT_POST, "txtIdTarefa"));
        $tarefa->setTitulo(filter_input(INPUT_POST, "txtTitulo"));
        $tarefa->setDescricao(filter_input(INPUT_POST, "txtDescricao"));
        $tarefa->autor->setId(filter_input(INPUT_POST, "selAutor"));
        $tarefa->setPrazofinal(filter_input(INPUT_POST, "txtPrazoFinal"));
        $tarefa->setStatus(filter_input(INPUT_POST, "selStatus"));

        echo $tarefaController->Edit($tarefa);
    }
    if($req == "3"){
        //Delete
        echo $tarefaController->Delete(filter_input(INPUT_POST, 'txtIdTarefa'));
    }
    if($req == "4"){
        echo $tarefaController->ListToDo();
    }

}