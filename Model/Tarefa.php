<?php
/**
 * Created by PhpStorm.
 * User: Alessandro
 * Date: 15/10/2018
 * Time: 13:50
 */

namespace Model;

require_once "Pessoa.php";

class Tarefa
{

    private $idtarefa;
    private $titulo;
    private $descricao;
    public $autor;
    private $prazofinal;
    private $datacadastro;
    private $status;
    private $autornome;

    /**
     * Tarefa constructor.
     * @param $autor
     */
    public function __construct()
    {
        $this->autor = new Pessoa();
    }

    /**
     * @return mixed
     */
    public function getIdtarefa()
    {
        return $this->idtarefa;
    }

    /**
     * @param mixed $idtarefa
     */
    public function setIdtarefa($idtarefa)
    {
        $this->idtarefa = $idtarefa;
    }

    /**
     * @return mixed
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param mixed $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * @return mixed
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param mixed $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

     /**
     * @return mixed
     */
    public function getPrazofinal()
    {
        return $this->prazofinal;
    }

    /**
     * @param mixed $prazofinal
     */
    public function setPrazofinal($prazofinal)
    {
        $this->prazofinal = $prazofinal;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDatacadastro()
    {
        return $this->datacadastro;
    }

    /**
     * @param mixed $datacadastro
     */
    public function setDatacadastro($datacadastro)
    {
        $this->datacadastro = $datacadastro;
    }

    /**
     * @return mixed
     */
    public function getAutornome()
    {
        return $this->autornome;
    }

    /**
     * @param mixed $autornome
     */
    public function setAutornome($autornome)
    {
        $this->autornome = $autornome;
    }



}