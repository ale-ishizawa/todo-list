<?php
/**
 * Created by PhpStorm.
 * User: Alessandro
 * Date: 15/10/2018
 * Time: 13:48
 */

namespace Model;


class Pessoa
{
    private $id;
    private $nome;

    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }


}