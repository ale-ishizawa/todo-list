<?php
/**
 * Created by PhpStorm.
 * User: Alessandro
 * Date: 15/10/2018
 * Time: 14:12
 */

include_once __DIR__. '/../View/Template/header.php';
include_once __DIR__. '/../View/Template/menu.php';

?>

    <div class="col-md-12 order-md-1">
    <h4 class="mb-3">Adicionar Nova Tarefa</h4>
    <form class="needs-validation" novalidate>
        <div class="row">
            <div class="col-md-6 mb-6">
                <label for="txtTitulo">Título</label>
                <input type="text" class="form-control" id="txtTitulo" placeholder="" value="" required>
                <div class="invalid-feedback">
                    Valid first name is required.
                </div>
            </div>
            <div class="col-md-6 mb-6">
                <label for="txtDescricao">Descrição</label>
                <input type="text" class="form-control" id="txtDescricao" placeholder="" value="" required>
                <div class="invalid-feedback">
                    Valid last name is required.
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 mb-4">
                <label for="txtAutor">Autor</label>
                <input type="hidden" name="txtIdAutor" id="txtIdAutor"/>
                <input type="text" class="form-control" id="txtAutor" name="txtAutor" placeholder="" value="" required>
            </div>
            <div class="col-md-4 mb-4">
                <label for="selStatus">Status</label>
                <select id="selStatus" class="form-control">
                    <option value="1">Pendente</option>
                    <option value="2">Em Andamento</option>
                    <option value="1">Concluído</option>
                </select>

            </div>
            <div class="col-md-4 mb-4">
                <label for="txtPrazofinal">Prazo Final</label>
                <input type="date" class="form-control" id="txtPrazofinal" placeholder="" value="" required>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 form-group">
                <div class="card-footer text-left">
                    <input type="button" class="btn btn-success" id="btnCadastrar" name="btnCadastrar" value="CADASTRAR"/>
                </div>
            </div>
        </div>
    </form>
    </div>
          <h2>Lista de Tarefas</h2>
          <div class="table-responsive">
            <table class="table table-striped table-sm">
              <thead>
                <tr>
                  <th>#Cód</th>
                  <th>Título</th>
                  <th>Descrição</th>
                  <th>Autor</th>
                  <th>Data de Cadastro</th>
                  <th>Prazo Final</th>
                  <th>Status</th>
                  <th>Excluir</th>
                </tr>
              </thead>
              <tbody id="tbody">

              </tbody>
            </table>
          </div>


<?php include_once __DIR__. '/../View/Template/footer.php'; ?>
<script type="text/javascript">
    $(function () {
        // Single Select AutoComplete
        $("#txtAutor").autocomplete({
            source: function (request, response) {
                // Fetch data
                $.ajax({
                    url: "Action/PessoaAC.php?req=4",
                    type: 'post',
                    dataType: "json",
                    data: {
                        search: request.term
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#txtAutor').val(ui.item.label); // display the selected text
                $('#txtIdAutor').val(ui.item.value); // save selected id to input
                return false;
            }
        });
    });
</script>
<script type="text/javascript" src="View/js/tarefa.js"></script>
