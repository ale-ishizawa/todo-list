$(document).ready(function () {
    $("#btnCadastrar").click(function () {
        var dados = {
            txtTitulo: $("#txtTitulo").val(),
            txtDescricao: $("#txtDescricao").val(),
            txtIdAutor: $("#txtIdAutor").val(),
            txtPrazoFinal: $("#txtPrazofinal").val(),
            selStatus: $("#selStatus").val()
        };

            $.ajax({
                url: "Action/TarefaAC.php?req=1",
                type: "post",
                dataType: "html",
                data: dados,
                success: function (result) {
                    $("#result").html(result);
                },
                error: function (result) {
                    console.log(result);
                }
            });
    });

    //Consulta para Listar todas as tarefas
    $.ajax({
        url: "Action/TarefaAC.php?req=4",
        dataType: "json",
        beforeSend: function () {
            //beforeSend é executado antes da chamada da requisição, pode ser usada para mostrar uma barra de carregameto
            $("#load").html("Carregando...");
        },
        complete: function () {
            //complete é executado quando a chamada é finalizada e seus dados retornados, pode ser usada para ocultar uma barra de carregameto
            $("#load").html("");
        },
        success: function (result) {
            //Se não houve nenhum erro na requisição, então esta função é executada.

            //https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/JSON/parse
            //var data = JSON.parse(result);//Como é retornado uma string JSON, convertemos ela para um objeto

            var tbody = document.getElementById("tbody");//Armazenamos o elemento a variável tbody
            tbody.innerHTML = ""; //Limpamos todo o conteúdo do elemento tbody

            for (i = 0; i < Object.keys(result).length; i++) {
                var tr = document.createElement("tr");//Criamos o elemento TR

                var tdId = document.createElement("td"); //Criamos o elemento TD para armazenar o ID
                tdId.innerHTML = result[i].Idtarefa; //Para o valor da coluna, será o atribuido o valor Id da posição no array de objetos
                tr.appendChild(tdId); //Inserimos o elemento TD dentro do elemento TR

                var tdTitulo = document.createElement("td");
                tdTitulo.innerHTML = result[i].Titulo;
                tr.appendChild(tdTitulo);

                var tdDescricao = document.createElement("td");
                tdDescricao.innerHTML = result[i].Descricao;
                tr.appendChild(tdDescricao);

                var tdAutor = document.createElement("td");
                tdAutor.innerHTML = result[i].Autornome;
                tr.appendChild(tdAutor);

                var tdPrazoFinal = document.createElement("td");
                tdPrazoFinal.innerHTML = result[i].Prazofinal;
                tr.appendChild(tdPrazoFinal);

                var tdDtCadastro = document.createElement("td");
                tdDtCadastro.innerHTML = result[i].Datacadastro;
                tr.appendChild(tdDtCadastro);

                var tdStatus = document.createElement("td"); //Criamos o elemento TD para armazenar o telefone
                tdStatus.innerHTML = result[i].Status;
                tr.appendChild(tdStatus); //Inserimos o elemento TD dentro do elemento TR

                var tdDelete = document.createElement("td");
                tdDelete.innerHTML = '<a class="btn btn-danger" onclick="return confirm('+"Deseja excluir a tarefa "+result[i].Titulo+');">Excluir</a> ';
                tr.appendChild(tdDelete);

                tbody.appendChild(tr);
            }
        },
        error: function (result) {
            //Se houve erro na requisição, então esta função é executada.
            console.log(result);
        }
    });
});

function Delete(idtarefa){

        alert("chegou aqui");
        console.log("ta certo");


}