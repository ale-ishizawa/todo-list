<?php
/**
 * Created by PhpStorm.
 * User: Alessandro
 * Date: 18/10/2018
 * Time: 14:51
 */

//namespace Controller;
require_once ("../DAL/PessoaDAO.php");
//use Model\Pessoa;
//use DAL\PessoaDAO;

class PessoaController
{
    private $pesDAO;

    public function __construct()
    {
        $this->pesDAO = new \DAL\PessoaDAO();
    }

    public function ListPessoas($search)
    {
        return $this->pesDAO->ListPessoas($search);
    }

}