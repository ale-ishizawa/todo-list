<?php
/**
 * Created by PhpStorm.
 * User: Alessandro
 * Date: 15/10/2018
 * Time: 13:54
 */
require_once "../DAL/TarefaDAO.php";
require_once "../Model/Tarefa.php";

class TarefaController
{

    private $tarDAO;


    public function __construct()
    {
        $this->tarDAO = new \DAL\TarefaDAO();
    }

    public function Save(\Model\Tarefa $tarefa)
    {
//        if($tarefa->getTitulo() != "" && $tarefa->autor->getId() != "" && $tarefa->getStatus() != ""){
            return $this->tarDAO->Save($tarefa);
//        }else{
//            return "Dados inválidos! Preencha todos os campos obrigatórios * corretamente.";
//        }
    }

    public function Edit(Tarefa $tarefa)
    {
        if($tarefa->getIdtarefa() > 0 && $tarefa->getTitulo() != "" && $tarefa->autor->getId() > 0 && $tarefa->getStatus() != ""){
            return $this->tarDAO->Save($tarefa);
        }else{
            return "Dados inválidos! Preencha todos os campos obrigatórios * corretamente.";
        }
    }

    public function Delete($idTarefa)
    {
        if($idTarefa > 0){
            return $this->tarDAO->Delete($idTarefa);
        }else{
            return "Id da tarefa inválido.";
        }
    }

    public function ListToDo()
    {
        return $this->tarDAO->ListToDo();
    }

}